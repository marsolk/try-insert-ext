# try-insert-ext

This library provides extension traits for `std` types, adding `or_try_insert_with` (or equivalent) methods for `Option`, map `Entry` types, and `HashSet`. These act similarly to `or_insert_with` methods, but taking a function that returns a `Result`, and inserting only if the function returns `Ok`, otherwise returning the `Err`.
