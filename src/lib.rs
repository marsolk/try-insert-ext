//! Provides extension traits for `Option` and map `Entry` types, adding
//! `try_insert` methods. These methods take possibly empty containers, and if
//! empty, run an initialization function. If this function errors, an error is
//! returned. Otherwise, the initialized value is returned.

#[cfg(feature = "std")]
mod entry;
mod option;
#[cfg(feature = "std")]
mod set;

#[cfg(feature = "std")]
pub use entry::EntryInsertExt;
pub use option::OptionInsertExt;
#[cfg(feature = "std")]
pub use set::SetInsertExt;
